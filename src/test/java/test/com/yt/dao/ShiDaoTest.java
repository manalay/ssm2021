package test.com.yt.dao;

import com.yt.dao.ShiDao;
import com.yt.entity.Shi;
import com.yt.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @Classname ShiDaoTest
 * @Description
 * @Date 2021/11/1 10:21
 * @Create by 杨涛
 */
public class ShiDaoTest {

    @Test
    public void testgetAll(){
        List<Shi> shis=shiDao.getAll();
        for(Shi shi:shis){
            System.out.println(shi);
        }
    }

    SqlSession session=null;
    ShiDao shiDao=null;
    @Before
    public void init(){
        session =  MyBatisUtil.createSqlSession();

         shiDao=session.getMapper(ShiDao.class);
    }
    @After
    public void end(){
        session.commit();
        MyBatisUtil.closeSqlSession(session);
    }
}
