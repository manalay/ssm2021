package com.yt;

import com.yt.dao.UserDao;
import com.yt.entity.User;
import com.yt.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Classname Test
 * @Description
 * @Date 2021/10/29 8:58
 * @Create by 杨涛
 */
public class Test {


    public static void main(String[] args) {

        SqlSession session = MyBatisUtil.createSqlSession();
        testupdateUser(session);
        session.commit();
        MyBatisUtil.closeSqlSession(session);
    }

    /**
     * 测试 用户登录时,使用 username 和 password 两个参数
     *
     * @param session
     */
    public static void test5(SqlSession session) {

        User u = session.getMapper(UserDao.class)
                .loginBynameAndpwd("wangzeng1","000000" );

        System.out.println(u);

    }

    public static void testaddUser(SqlSession session) {
        UserDao userDao=session.getMapper(UserDao.class);
        User u=new User();
        u.setUsername("wangzeng1");
        u.setPassword("000000");
        u.setNickname("王增");
        u.setLevel(100);
        int line  = userDao.addUser(u) ;
        System.out.println("影响了：" +
                line+"行");
    }

    public static void testupdateUser(SqlSession session) {
        UserDao userDao=session.getMapper(UserDao.class);
        User u=new User();
        u.setId(16);
        u.setUsername("wangzeng2");
        u.setPassword("111111");
        u.setNickname("王增2");
        u.setLevel(99);
        int line  = userDao.updateUser( u) ;
        System.out.println("影响了：" +
                line+"行");
    }
    /**
     * 测试 用户登录时,使用 Map 对象做为参数
     *
     * @param session
     */
    public static void test4(SqlSession session) {
        Map map=new HashMap<String,String>();
        map.put("name","admin");
        map.put("password","admin");
        User u = session.getMapper(UserDao.class).loginByMap(map );

        System.out.println(u);

    }
    /**
     * 测试 用户登录时,使用 User 对象做为参数
     *
     * @param session
     */
    public static void test3(SqlSession session) {
        User u = new User();
        u.setUsername("王宇翔");
        u.setPassword("123");
        u = session.getMapper(UserDao.class).login(u);

        System.out.println(u);

    }

    /**
     * 测试模糊查询
     *
     * @param session
     */
    public static void test2(SqlSession session) {
        List<User> userlist = session.getMapper(UserDao.class).getUsersByName("人");
        for (User u : userlist) {
            System.out.println(u);
        }
    }

    /**
     * 第一节课测试
     *
     * @param session
     */
    public static void test1(SqlSession session) {

        List<User> userlist = session.getMapper(UserDao.class).getAllUsers();
        for (User u : userlist) {
            System.out.println(u);
        }
    }

}
