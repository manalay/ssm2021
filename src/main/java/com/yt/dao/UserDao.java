package com.yt.dao;

import com.yt.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Classname UserDao
 * @Description
 * @Date 2021/10/29 10:20
 * @Create by 杨涛
 */
public interface UserDao {
    List<User> getAllUsers();

    List<User> getUsersByName(String username);

    User login(User u);
    User loginByMap(Map map);

    User loginBynameAndpwd(@Param("username") String username,
                           @Param("password") String password);
    int addUser(User u);
    int updateUser(User u);
}
