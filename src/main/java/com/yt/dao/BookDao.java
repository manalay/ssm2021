package com.yt.dao;

import com.yt.entity.Book;

import java.util.List;

/**
 * @Classname BookDao
 * @Description
 * @Date 2021/11/2 14:16
 * @Create by 杨涛
 */
public interface BookDao {
    List<Book> getBooksByBook(Book book);
    void addBook(Book book);
}
