package com.yt.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname Guo
 * @Description
 * @Date 2021/11/1 9:59
 * @Create by 杨涛
 */
@Data
@EqualsAndHashCode
public class Guo {
    private int id;
    private String name;
    private List<Sheng> shengs=new ArrayList<>();

}
