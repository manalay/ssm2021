package com.yt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Classname Book
 * @Description
 * @Date 2021/11/2 14:11
 * @Create by 杨涛
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    private Integer bid;
    private String bookname;
    private String publish;
    private String author;
    private String version;
    private Date publishdate;
    private Float price;
}
