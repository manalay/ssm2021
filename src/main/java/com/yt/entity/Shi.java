package com.yt.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Classname Shi
 * @Description
 * @Date 2021/11/1 9:59
 * @Create by 杨涛
 */
@Data
@EqualsAndHashCode
public class Shi {
    private Sheng sheng;
    private int id;
    private String name;
}
