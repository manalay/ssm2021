package com.yt.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname Sheng
 * @Description
 * @Date 2021/11/1 9:59
 * @Create by 杨涛
 */
@Data
@EqualsAndHashCode
public class Sheng {
    private int id;
    private String name;
    private Guo guo;
    private List<Shi> shis=new ArrayList<>();
}
