package com.yt.ch01.type1.util;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.yt.ch01.type1.entity.Student;

public class Test {
	public static void main(String[] args) {
		ApplicationContext ac=
			//new ClassPathXmlApplicationContext("ch01/type1/student.xml");
			new ClassPathXmlApplicationContext("spring/student1.xml");
		Student stu=(Student) ac.getBean("ch01.type1.entity.Student");
		System.out.println(stu);
	}

}
