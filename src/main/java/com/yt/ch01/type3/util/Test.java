package com.yt.ch01.type3.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yt.ch01.type3.entity.Student;

public class Test {
	public static void main(String[] args) {
		ApplicationContext ac=
			new ClassPathXmlApplicationContext("spring/student3.xml");
		Student stu=(Student) ac.getBean("stu");
		System.out.println(stu);
	}
}
