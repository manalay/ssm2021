package com.yt.aop;

/**
 * @Classname MysqlTeacher
 * @Description
 * @Date 2021/11/3 8:31
 * @Create by 杨涛
 */
public class MysqlTeacher implements Teacher{
    @Override
    public void teach() {
        System.out.println("讲mysql");
    }
}
