package com.yt.aop;

/**
 * @Classname ProxyTeacher
 * @Description
 * @Date 2021/11/3 8:27
 * @Create by 杨涛
 */
public class ProxyTeacher implements Teacher{

    private Teacher teacher;
    public ProxyTeacher(Teacher teacher){
        this.teacher=teacher;
    }

    public void begin() {
        System.out.println("开投影");
    }
    @Override
    public void teach() {
        begin();
        teacher.teach();
        end();
    }
    public void end(){
        System.out.println("关投影");

    }
}
