package com.yt.aop;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname Student
 * @Description
 * @Date 2021/11/3 8:55
 * @Create by 杨涛
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String name;
}
